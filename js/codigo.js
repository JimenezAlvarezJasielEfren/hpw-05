
function asociar_hijo(obj,padre)
{
    padre.appendChild(obj);
}

function crea_form(componentes){
    var form_temporal=document.createElement("form");
    form_temporal.setAttribute("role","form");
    for(var i=0;i<componentes.length;i++){
        var div_temp=document.createElement("div");
        div_temp.setAttribute("class","form-group");
        asociar_hijo(componentes[i]["etiqueta"],div_temp);
        asociar_hijo(componentes[i]["input"],div_temp);
        asociar_hijo(div_temp,form_temporal);
    }
    
    return form_temporal;
}

function crear_input_text(name,etiqueta,id){
    var obj;
    var input_temporal=document.createElement("input");
    input_temporal.setAttribute("type","text");
    input_temporal.setAttribute("name",name);
    input_temporal.setAttribute("id",id);
    input_temporal.setAttribute("class","form-control");
    var etiqueta_temporal=document.createElement("label");
    etiqueta_temporal.textContent=etiqueta;
    etiqueta_temporal.setAttribute("for",id);
    etiqueta_temporal.setAttribute("class","lead");
    obj={"etiqueta":etiqueta_temporal,"input":input_temporal};
    return obj;
}


function crear_input_mail(name,etiqueta,id){
    var obj;
    var input_temporal=document.createElement("input");
    input_temporal.setAttribute("type","mail");
    input_temporal.setAttribute("name",name);
    input_temporal.setAttribute("id",id);
    input_temporal.setAttribute("class","form-control");
    var etiqueta_temporal=document.createElement("label");
    etiqueta_temporal.textContent=etiqueta;
    etiqueta_temporal.setAttribute("for",id);
    etiqueta_temporal.setAttribute("class","lead");
    obj={"etiqueta":etiqueta_temporal,"input":input_temporal};
    return obj;
}


function crea_select(name,etiqueta,id,opciones){
    var select_temp=document.createElement("select");
    for(var i=0;i<opciones.length;i++){
        var opcion=document.createElement("option");
        opcion.setAttribute("value",opciones[i]);
        opcion.textContent=opciones[i];
        asociar_hijo(opcion,select_temp);
    }
    var etiqueta_temporal=document.createElement("label");
    etiqueta_temporal.textContent=etiqueta;
    etiqueta_temporal.setAttribute("class","lead");
    var obj={"etiqueta":etiqueta_temporal,"input":select_temp};
    return obj;
}

function ubicar_elementos_row(c){
        var ra=document.createElement("div");
         ra.setAttribute("class","row");
    for(var i=0;i<c.length;i++){
         var co=document.createElement("div");
         co.setAttribute("class",c[i]["columna"]);
         asociar_hijo(c[i]["elemento"],co);
         asociar_hijo(co,ra);
    }
         return ra;
}


function ubicar_elemento_row(elemento,col){
         var r=document.createElement("div");
         r.setAttribute("class","row");
         var co=document.createElement("div");
         co.setAttribute("class",col);
         asociar_hijo(elemento,co);
         asociar_hijo(co,r);
         return r;
}

var arr=[crear_input_text("nombre","Nombre: ","fornombre"),crear_input_text("comida","Comida preferida: ","forcomida"),crear_input_mail("mail","Correo electronico: ","formail"),crea_select("sexo","Sexo: ","idsex",["Masculino","Femenino"])];
var fo=crea_form(arr);
var foo=crea_form(arr);

var elementos=[{"columna":"span6","elemento":fo},{"columna":"span6","elemento":foo}];
var contenedor=document.createElement("div");
contenedor.setAttribute("class","container");
var r=ubicar_elementos_row(elementos);
asociar_hijo(r,contenedor);
document.body.appendChild(contenedor);
//Layouts && CSS
//(Bootstrap)
//ExtJs
//JQuery



